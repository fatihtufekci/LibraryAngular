> GROOVY/GRAILS - ANGULAR ile Kütüphane Sistemi

**Proje Detayları:** Bu projede kütüphane sistemi geliştirilmiştir. Personel kütüphaneye kitap ekleyebilir. Kitabın kategorisi ve yazarları seçilir. Kütüphanedeki kitaplar aranabilir ve kitaplar listenebilir. Kitapların bilgilerini güncelleyip, kütüphanedeki kitapları silebilir. Kategori oluşturup, arama yapabilir. Kategoriyi listeleyebilir, kategori bilgilerini güncelleyebilir ve kategorileri silebilir. Personel yazar ekleyebilir, listeleyebilir, güncelleyebilir ve silebilir.


**Ekran Alıntıları:** (https://gitlab.com/fatihtufekci/LibraryAngular/tree/master/screenshots)