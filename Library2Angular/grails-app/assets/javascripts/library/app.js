var app = angular.module("myApp", []);

app.controller("BookController", function ($scope, $rootScope, $http, $location, $timeout) {
    var myVm = this;
    myVm.insertupdate = insertupdate;
    myVm.load = load;
    myVm.getbookcategorylist = getbookcategorylist;
    myVm.getauthorslist = getauthorslist;
    myVm.deletebook = deletebook;
    myVm.getbookinfo = getbookinfo;

    myVm.buttonName = "Create";

    myVm.load();
    myVm.getbookcategorylist();
    myVm.getauthorslist();

    myVm.myDate = new Date();
    myVm.isOpen = false;


    function load(){
        $http.get("http://localhost:8080/book/getbooklist")
            .then(function (response) {
            myVm.books = response.data;
        });
    }

    function getauthorslist() {
        $http.get("http://localhost:8080/author/getauthorlist").then(function (response) {
            myVm.authors = response.data;
        });
    }

    function getbookcategorylist() {
        $http.get("http://localhost:8080/bookcategory/getir").then(function (response) {
            myVm.bookCategory = response.data
        });
    }

    function getbookinfo(bookId){
        $http.post('http://localhost:8080/book/getbookinfo',data={id:bookId})
            .then(function (response){
                myVm.bookInstance = response.data.bookInstance;
                myVm.buttonName = "Update";
                myVm.errorMessage = "";
            });
    }

    function deletebook(bookId){
        $http.post('http://localhost:8080/book/deletebook', data={id:bookId})
            .then(function (response) {
                if(response.data.messageType=="info"){
                    myVm.load();
                }

            });
    }

    function insertupdate(){
        $http.post('http://localhost:8080/book/insertupdate', data = {bookInstance: myVm.bookInstance})
            .then(function (response){
                myVm.errorMessage = "";
                if (response.data.messageType == "info"){
                    myVm.bookInstance = {};
                    myVm.load();
                }else if (response.data.messageType == "error") {
                    myVm.errorMessage = response.data.message;
                }
                myVm.buttonName = "Create"
            });
    }
});

app.controller("AuthorController", function ($scope, $rootScope, $http, $location, $timeout) {

    var myVm = this;
    myVm.load = load;
    myVm.insertupdate = insertupdate;
    myVm.getauthorinfo = getauthorinfo;
    myVm.deleteauthor = deleteauthor;

    myVm.buttonName = "Create";

    myVm.load();

    function load() {
        $http.get("http://localhost:8080/author/getauthorlist").then(function (response) {
            myVm.authors = response.data;
        });
    }

    function getauthorinfo(authorId) {
        $http.post("http://localhost:8080/author/getauthorinfo", data={id:authorId}).then(function (response) {
            myVm.authorInstance = response.data.authorInstance;
            myVm.buttonName = "Update";
            myVm.errorMessage = "";
        });
    }

    function deleteauthor(authorId) {
        $http.post("http://localhost:8080/author/deleteauthor", data={id:authorId}).then(function (response) {
            if(response.data.messageType=="info"){
                myVm.load();
            }
        });
    }

    function insertupdate() {
        $http.post("http://localhost:8080/author/insertupdate", data={authorInstance: myVm.authorInstance}).then(function (response) {
            myVm.errorMessage = "";
            if (response.data.messageType == "info"){
                myVm.authorInstance = {};
                myVm.load();
            }else if (response.data.messageType == "error") {
                myVm.errorMessage = response.data.message;
                $timeout(function () {
                    myVm.errorMessage = "";
                }, 2000);
            }
            myVm.buttonName = "Create"
        });
    }

})

app.controller("Bookcategory", function ($scope, $rootScope, $http, $location, $timeout) {
    var myVm = this;
    myVm.edit = edit;
    myVm.load = load;
    myVm.kaydet = kaydet;
    myVm.getbookcategoryInfo = getbookcategoryInfo;
    myVm.deletebookcategory = deletebookcategory;

    myVm.buttonName = "Create";

    myVm.load();

    function load() {
        //alert($rootScope.appUrl)
        myVm.errorMessage = "";
        $http.post('http://localhost:8080/bookcategory/getir')
            .then(function (data) {
                myVm.bookcategories = data.data;
                //angular.copy(myVm.books, myVm.copy);
            });
    }
    function getbookcategoryInfo(bookId,index) {
        $http.post('http://localhost:8080/bookcategory/getbookcategoryinfo',data={id:bookId})
            .then(function (data) {
                myVm.bookcategoryInstance = data.data.bookcategoryInfo;
                myVm.buttonName = data.data.buttonName;
                myVm.errorMessage = "";
            });
    }
    /*var bookcategoryInstance = $.param({
        bookcategoryName : myVm.bookcategoryName;
    })
    */

    function deletebookcategory(bookcategoryId) {
        $http.post('http://localhost:8080/bookcategory/deletebookcategory', data={id:bookcategoryId})
            .then(function (data) {
                if(data.data.messageType=="info"){
                    myVm.load();
                }

            });
    }

    function kaydet() {
        $http.post('http://localhost:8080/bookcategory/kaydet', data = {bookcategoryInstance: myVm.bookcategory})
            .then(function (data) {
                if (data.data.messageType == "info") {
                    myVm.bookcategoryInstance = {};
                    myVm.load();
                    myVm.errorMessage = data.data.message;
                } else if (data.data.messageType == "error") {
                    myVm.errorMessage = data.data.message;
                }
            });
    }
    function edit() {
        $http.post('http://localhost:8080/bookcategory/edit', data = {bookcategoryInstance: myVm.bookcategoryInstance})
            .then(function (data) {
                myVm.errorMessage = "";
                if (data.data.messageType == "info"){
                    myVm.nameId = data.data.message;
                    myVm.bookcategoryInstance = {};
                    myVm.load();
                }else if (data.data.messageType == "error"){
                    myVm.errorMessage = data.data.message;
                    $timeout(function () {
                        myVm.errorMessage = "";
                    }, 1500);
                }
                myVm.buttonName = "Create"
            });
    }

});