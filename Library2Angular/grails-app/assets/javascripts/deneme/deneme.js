// Burada [] -> dependecies injection içindir. Diğer controller'ımızı tanımlarken koyarız.
var deneme = angular.module("App", []);

deneme.controller("Deneme1Controller", function($scope){

    $scope.arabalar = [
        {"ad":"Mercedes","yil":2000},
        {"ad":"BMW","yil":2008},
        {"ad":"Ferrari","yil":2010},
        {"ad":"Audi","yil":2014},
        {"ad":"Honda","yil":2017}];

})

deneme.controller("MainController", function ($scope) {

    $scope.kullanicilar = [
        {"isim":"Fatih", "yas":21},
        {"isim":"Yunus", "yas":22},
        {"isim":"Yusuf", "yas":23}];

    $scope.YeniKullanici = function(){
        //alert("Fonksiyon çalıştı.");
        $scope.kullanicilar.push({
            isim:$scope.isim1,
            yas:$scope.yas1
        });

        $scope.isim1 =""
        $scope.yas1 = ""
    }
});

