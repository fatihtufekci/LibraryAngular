package library2angular

import grails.gorm.services.Service

@Service(Bookcategory)
interface BookcategoryService {

    Bookcategory get(Serializable id)

    List<Bookcategory> list(Map args)

    Long count()

    void delete(Serializable id)

    Bookcategory save(Bookcategory bookcategory)

}