package library2angular

class Author {

    int id
    String authorName
    String authorSurname

    static hasMany = [books: Book]
    static mapping = {
        sort "authorName"
    }

    static constraints = {

        authorName blank:false, nullable:false
        authorSurname blank:false, nullable:false
    }
    String toString(){
        return authorName
    }

}
