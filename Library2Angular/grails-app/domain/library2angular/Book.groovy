package library2angular

class Book {

    int id
    String bookName
    String bookIsbn
    Date bookPublishdate
    //boolean bookIspublished
    String bookDescription

    Bookcategory bookCategory

    static belongsTo = [Author]

    static hasMany = [authors: Author]


    static constraints = {
        bookName blank:false, nullable: false
        bookIsbn blank:false, nullable:false
        bookPublishdate nullable:true
        //bookIspublished nullable:true
        bookDescription nullable: false, blank: true, type:'text', maxSize: 1000
        authors nullable: true
        bookCategory nullable: true
    }

    def convertToMap(){
        def authorList=this.authors.collect {it.id.toString()}
        def resultMap=[id:this.id
                       ,bookName:this.bookName
                ,bookPublishdate:this.bookPublishdate?.format("dd.MM.yyyy")
                       , bookCategory:this.bookCategory?[id:this.bookCategory.id.toString(),bookCategoryGhostName:this.bookCategory.bookcategoryName ]:null
                ,authors:authorList
        ]
        return resultMap
    }

}
