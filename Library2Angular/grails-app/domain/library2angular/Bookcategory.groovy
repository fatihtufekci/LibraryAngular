package library2angular

class Bookcategory {

    long id
    String bookcategoryName

    static hasMany = [books: Book]

    static constraints = {
        bookcategoryName blank:false,nullable: false
    }

}
