package library2angular

import grails.converters.JSON
import grails.gorm.transactions.Transactional
import org.hibernate.Transaction

import javax.servlet.http.HttpSession


class BookcategoryController {

    BookcategoryService bookcategoryService

    def index() {

    }

    def edit2(){}
    def merhaba(){}

    /*
    def getbookcategoryList(){
        def  bookcategoryList = Bookcategory.list()
        render bookcategoryList as JSON
    }
    */

    def getir(){
        def liste = bookcategoryService.list(params)
        render liste as JSON
    }

    def getbookcategoryinfo(){
        def bookcategoryId=request.JSON.id
        def bookcategoryInfo = bookcategoryService.get(bookcategoryId)
        def resultMap = [messageType: "info", message: "", buttonName:"Update", bookcategoryInfo:bookcategoryInfo]
        render resultMap as JSON
    }

    def deletebookcategory(){
        def bookcategoryId = -1
        bookcategoryId = request.JSON.id
        def resultMap
        if(bookcategoryId != -1){
            resultMap = [messageType: "info", message: "İşlem başarıyla gerçekleşti"]
            bookcategoryService.delete(bookcategoryId)
        }else{
            resultMap = [messageType: "error", message: "İşlem gerçekleşemedi.."]
        }
        render resultMap as JSON
    }

    def kaydet(){
        def bookInstanceJson = request.JSON.bookcategoryInstance
        def bookcategoryInstance = new Bookcategory()
        bookcategoryInstance.properties = bookInstanceJson

        def resultMap

        if(bookcategoryInstance.validate()){
            bookcategoryService.save(bookcategoryInstance)
            if(bookcategoryInstance.errors?.errorCount > 0){
                resultMap = [messageType:"error", message:"İşlem gerçekleşmedi.."]
            }else{
                resultMap = [messageType:"info", message:"İşlem gerçekleşti.."]
            }
        }else{
            resultMap = [messageType:"error", message:"İşlem gerçekleşmedi.."]
        }

        render resultMap as JSON
    }


    def edit(){
        def bookcategoryJson = request.JSON.bookcategoryInstance
        def bookcategoryInstance
        def resultMap = [messageType:"error", message:"İşlem gerçekleşmedi.."]

        if(bookcategoryJson != null){
            if(bookcategoryJson.id){
                bookcategoryInstance = bookcategoryService.get(bookcategoryJson.id)
                bookcategoryInstance.properties = bookcategoryJson
            }else{
                bookcategoryInstance = new Bookcategory()
                bookcategoryInstance.properties = bookcategoryJson
            }

            if(bookcategoryInstance.validate()){
                bookcategoryService.save(bookcategoryInstance)
                if(bookcategoryInstance.errors?.errorCount > 0){
                    resultMap = [messageType:"error", message:"İşlem gerçekleşmedi.."]
                }else{
                    resultMap = [messageType:"info", message:"İşlem gerçekleşti.."]
                }
            }else{
                resultMap = [messageType:"error", message:"İşlem gerçekleşmedi.."]
            }
        }else{
            resultMap = [messageType:"error", message:"İşlem gerçekleşmedi.."]
        }

        render resultMap as JSON
    }

}
