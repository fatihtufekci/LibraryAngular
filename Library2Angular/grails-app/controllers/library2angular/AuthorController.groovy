package library2angular

import grails.converters.JSON

class AuthorController {

    AuthorService authorService

    def index() { }

    def getauthorlist(){
        def liste = authorService.list(params)
        render liste as JSON
    }

    def getauthorinfo(){
        def authorId = -1
        authorId = request.JSON.id
        def resultMap
        if(authorId != -1){
            def authorInstance = authorService.get(authorId)
            if(authorInstance!=null){
                resultMap = [messageType:"info", message:"İşlem başarıyla gerçekleşti..", authorInstance:authorInstance]
            }
        }else{
            resultMap = [messageType:"error", message:"Request'ten id değeri alınamadı.!", authorInstance:null]
        }
        render resultMap as JSON
    }

    def deleteauthor(){
        def authorId = -1
        authorId = request.JSON.id
        def resultMap
        if(authorId != -1){
            authorService.delete(authorId)
            resultMap = [messageType:"info", message:"İşlem başarıyla gerçekleşti.."]
        }else{
            resultMap = [messageType:"error", message:"İşlem Başarısız.!"]
        }
        render resultMap as JSON
    }

    def insertupdate(){
        def authorJson = request.JSON.authorInstance
        def authorInstance
        def resultMap

        if(authorJson != null){
            if(authorJson.id){
                authorInstance = authorService.get(authorJson.id)
                authorInstance.properties = authorJson
            }else{
                authorInstance = new Author()
                authorInstance.properties = authorJson
            }

            if(authorInstance.validate()){
                authorService.save(authorInstance)
                if(authorInstance.errors?.errorCount > 0){
                    resultMap = [messageType:"error", message:"İşlem başarısız..!!"]
                }else{
                    resultMap = [messageType:"info", message:"İşlem başarıyla gerçekleşti.."]
                }
            }else{
                resultMap = [messageType:"error", message:"İşlem başarısız..!!"]
            }
        }else{
            resultMap = [messageType:"error", message:"Request'ten dönen nesne null..!"]
        }
        render resultMap as JSON
    }
}
