package library2angular

import grails.converters.JSON


class BookController {

    BookService bookService

    def index() { }

    def getbooklist(){
        def liste = bookService.list(params)
        def resultArr=liste.collect{it.convertToMap()}
        render resultArr as JSON
    }

    def login(){

    }

    def getbookinfo(){
        def bookId = -1
        bookId = request.JSON.id
        def resultMap
        if(bookId!=-1){
            def bookInstance = bookService.get(bookId)
            if(bookInstance != null){
                resultMap = [messageType:"info", message:"İşlem Başarıyla Gerçekleşti", bookInstance:bookInstance?.convertToMap()]
            }
        }else{
            resultMap = [messageType:"error", message:"İşlem Başarısız.!!", bookInstance:null]
        }
        render resultMap as JSON
    }

    def deletebook(){
        def bookId = -1
        bookId = request.JSON.id
        def resultMap
        if(bookId != -1){
            bookService.delete(bookId)
            resultMap = [messageType:"info", message:"İşlem Başarılı"]
        }else{
            resultMap = [messageType:"error", message:"İşlem Başarısız.!!"]
        }
        render resultMap as JSON
    }

    def insertupdate(){
        def bookJson = request.JSON.bookInstance
        def bookInstance
        def resultMap
        if(bookJson!=null){
            // Yeni oluşmuşsa id'si olmaz, bizde else'te create ederiz.
            // Ancak id'si varsa update işlemi yaparız.
            if(bookJson.id){
                bookInstance = bookService.get(bookJson.id)
                bookInstance.properties = bookJson
            }else{
                bookInstance = new Book()
                bookInstance.properties = bookJson
            }

            if(bookInstance.validate()){
                //bookInstance.removeFromAuthors()
                bookJson.authors.each{
                    def authorInstance = Author.read(it)
                    bookInstance.removeFromAuthors(authorInstance)
                    bookInstance.addToAuthors(authorInstance)
                }
                bookService.save(bookInstance)
                if(bookInstance.errors?.errorCount > 0){
                    resultMap = [messageType:"error", message:"İşlem gerçekleşmedi.."]
                }else{
                    resultMap = [messageType:"info", message:"İşlem gerçekleşti.."]
                }

            }else{
                resultMap = [messageType:"error", message:"İşlem gerçekleşmedi.."]
            }

        }else{
            resultMap = [messageType:"error", message:"İşlem gerçekleşmedi.."]
        }

        render resultMap as JSON
    }
}
