<!doctype html>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Author</title>
</head>
<body>

<div class="container" ng-app="myApp" ng-controller="AuthorController as myVm">

    <legend><h1>Add Author</h1></legend>
    <br/>
    <span style="color: #FF0000">{{myVm.errorMessage}}</span>
    <form method="post">
        <input class="form-control" type="text" ng-model="myVm.authorInstance.authorName" placeholder="Author Name" value="{{myVm.authorInstance.authorName}}"><br/>
        <input class="form-control" type="text" ng-model="myVm.authorInstance.authorSurname" placeholder="Author SurName" value="{{myVm.authorInstance.authorSurname}}"><br/>
        <input type="hidden" ng-model="myVm.authorInstance.id" value="{{myVm.authorInstance.id}}">
        <button class="btn btn-primary" type="button" ng-click="myVm.insertupdate()">{{myVm.buttonName}}</button>
    </form>
    <br/>

    <div>
        <table class="table table-hover">
            <thead class="thead-light">
            <tr>
                <th scope="col" style="color: black">Author Name</th>
                <th scope="col" colspan="2" style="color: black">Author Surname</th>
            </tr>
            </thead>
            <tbody>
            <tr ng-repeat="authorIns in myVm.authors">
                <td><span style="font-size: 18px">{{authorIns.authorName}}</span></td>
                <td><span style="font-size: 18px">{{authorIns.authorSurname}}</span></td>
                <form>
                    <td><button ng-click="myVm.getauthorinfo(authorIns.id)" type="button" class="btn btn-info">Edit</button>
                        <button type="button" class="btn btn-danger" ng-click="myVm.deleteauthor(authorIns.id)">Delete</button>
                    </td>
                </form>
            </tr>
            </tbody>
        </table>

    </div>

</div>

</body>
</html>