<%@ page import="library2angular.Author" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main" />
    <title>Book</title>
</head>
<body>

<div class="container" ng-app="myApp" ng-controller="BookController as myVm">

<legend><h1>ADD A BOOK</h1></legend>

<br/>
<form method="post">
    <div class="form-row">
        <div class="form-group col-md-6">
            <input type="text" ng-model="myVm.bookInstance.bookName" placeholder="Book Name" class="form-control" value="{{myVm.bookInstance.bookName}}"/><br/>
        </div>
        <div class="form-group col-md-6">
            <input type="text" ng-model="myVm.bookInstance.bookIsbn" placeholder="Book Isbn" class="form-control" value="{{myVm.bookInstance.bookIsbn}}"/><br/>
        </div>
    </div>
    <div class="form-group">
        <label style="font-size: 18px; font-weight: bold; font-family:'Arial'">Category of Book</label>
        <select class="form-control" ng-model="myVm.bookInstance.bookCategory.id">
            <option ng-repeat="x in myVm.bookCategory" value="{{x.id}}">{{x.bookcategoryName}}</option>
        </select>
    </div>
    <div class="form-group">
        <label style="font-size: 18px; font-weight: bold">Author of Book</label>
        %{--{{myVm.bookInstance.authors}}--}%
        %{--<g:select multiple="" name="authors" from="${library2angular.Author.list()}" value="{{myVm.bookInstance.authors}}" ng-model="myVm.bookInstance.authors"/>--}%
        <select multiple class="form-control" ng-model="myVm.bookInstance.authors">
            <option ng-repeat="x in myVm.authors" value="{{x.id}}">{{x.authorName + " " + x.authorSurname}}</option>
        </select>
    </div>
    <div class="form-group">
        <label style="font-size: 18px; font-weight: bold">Book Publish Date</label>
        <input class="form-control" type="date" id="date" value="{{myVm.bookInstance.bookPublishdate}}" ng-model="myVm.bookInstance.bookPublishdate"/>
    </div>
    <div class="form-group">
        <input type="text" ng-model="myVm.bookInstance.bookDescription" placeholder="Book Description" class="form-control" value="{{myVm.bookInstance.bookDescription}}"/><br/>
    </div>
    <input type="hidden" ng-model="myVm.bookInstance.id" value="{{myVm.bookInstance.id}}">
    <button class="btn btn-primary" type="button" ng-click="myVm.insertupdate()">{{myVm.buttonName}}</button>

</form>

<br/>

<legend>Search for Book</legend>
<div class="form-group">
    <form class="form-inline" method="post">
        <input type="search" ng-model="filtrele" style="font-size: 18px; font-weight: bold" placeholder="Book Name" class="form-control"/>
    </form>
</div>

<br/>

<table class="table table-hover">
    <thead class="thead-dark">
    <tr>
        <th scope="col" >Book Name</th>
        <th style="width:200px;"></th>
    </tr>

    </thead>
    <tbody>
    <tr ng-repeat="x in myVm.books | filter: filtrele">
            <td><span style="font-size: 18px">{{x.bookName}} {{x.bookCategory.bookCategoryGhostName}}</span></td>
            <form method="post">
                <td>
                    <button class="btn btn-outline-warning" type="button" ng-click="myVm.getbookinfo(x.id)">Edit</button>
                    <button class="btn btn-outline-danger" type="button" ng-click="myVm.deletebook(x.id)">Delete</button>
                </td>

            </form>
    </tr>
    </tbody>
</table>

</div>
</body>
</html>