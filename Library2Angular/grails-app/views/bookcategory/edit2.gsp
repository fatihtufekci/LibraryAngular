<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>EDİT</title>
</head>
<body>

<div ng-app="myApp" ng-controller="EditBookCtrl">
    <form class="form-horizontal">
        <h1 class="page-header">Edit Bookcategory({{bookcategoryInstance.id}})</h1>
        <fieldset>
            <div class="control-group">
                <label class="control-label" for="bookcategoryName">Book Category Name</label>
                <div class="controls">
                    <input id="bookcategoryName" type="text" class="input-block-level"
                           required="true" ng-model="bookcategoryInstance.bookcategoryName"></input>
                </div>
            </div>

        </fieldset>
        <div class="form-actions">
            <button ng-click="updateBook()" class="btn btn-primary">Update Book</button>
        </div>
    </form>
</div>

</body>
</html>