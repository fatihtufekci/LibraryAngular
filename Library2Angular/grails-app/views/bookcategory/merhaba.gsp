<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main"/>
    <g:set var="entityName" value="${message(code: 'book.label', default: 'Book')}" />
    <title><g:message code="default.list.label" args="[entityName]" /></title>
</head>
<body>
<br/>
<div  ng-app="App">
    <div>
        <g:textField class="" name="fatih" ng-model="isim"/>
        {{"Merhaba " + isim}}
    </div>
    <br/><br/>

    <div ng-init="isimler=['Fatih','Yunus','Yusuf']">
        <ul>
            <li ng-repeat="i in isimler">
                {{i}}
            </li>
        </ul>
    </div>
    <br/><br/>
    <div ng-controller="MainController">
        <ul>
            <li ng-repeat="i in kullanicilar">
                {{i.isim}} - {{i.yas}}
            </li>
        </ul>
        <br/><br/>

        <form ng-submit="YeniKullanici()">
            <g:textField name="isim1" ng-model="isim1" placeholder="isim"/>
            <g:textField name="yas1" ng-model="yas1" placeholder="yas"/>
            <g:submitButton name="buton"/>
        </form>

    </div>
    <br/><br/>
    <div ng-controller="Deneme1Controller">
        <g:textField name="filtre" ng-model="filtrele" placeholder="Yazmaya başlayın.."/>
        <ul>
            <li ng-repeat="araba in arabalar | filter: filtrele">
                {{araba.ad}} - {{araba.yil}}
            </li>
        </ul>
    </div>


</div>


</body>
</html>