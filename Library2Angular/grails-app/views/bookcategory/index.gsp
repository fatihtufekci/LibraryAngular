<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main" />
    <g:set var="entityName" value="${message(code: 'bookcategory.label', default: 'Bookcategory')}" />
    <title><g:message code="default.list.label" args="[entityName]" /></title>

</head>
<body>
<div class="container" ng-app="myApp" ng-controller="Bookcategory as myVm">

    <legend><h1>Add Category</h1></legend>
    <br/>
    <span style="color: #FF0000">{{myVm.errorMessage}}</span>
    <form method="post">
        <input type="text" ng-model="myVm.bookcategoryInstance.bookcategoryName" placeholder="Category Name" value="{{myVm.bookcategoryInstance.bookcategoryName}}">
        <input type="hidden" ng-model="myVm.bookcategoryInstance.id" value="{{myVm.bookcategoryInstance.id}}">
        <button class="btn btn-primary" type="button" ng-click="myVm.edit()">{{myVm.buttonName}}</button>
    </form>
    <br/>

     <div>
           <table class="table table-hover">
                <thead class="thead-light">
                        <tr>
                            <th scope="col" colspan="3" style="color: black">Category Name</th>
                        </tr>
                 </thead>
                 <tbody>
                    <tr ng-repeat="bookcategoryIns in myVm.bookcategories">
                            <td>{{bookcategoryIns.bookcategoryName}}</td>
                            <form>
                                <td><button ng-click="myVm.getbookcategoryInfo(bookcategoryIns.id,$index)" type="button" class="btn btn-primary">Edit</button>
                                    <button type="button" class="btn btn-danger" ng-click="myVm.deletebookcategory(bookcategoryIns.id)">Delete</button>
                                </td>
                            </form>
                        </tr>
                </tbody>
           </table>

     </div>

</div>
</body>
</html>